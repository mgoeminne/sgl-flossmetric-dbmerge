#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys
import argparse
import yaml
import urllib2
import bs4
import logging

def parse_args():
    desc = "Parse"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("-u", "--url", default="http://git.gnome.org",
                        help="Root URL of repositories list.")
    parser.add_argument("-o", "--out", default="gnome.yml",
                        help="YAML out file where to store the result.")
    return parser.parse_args()


def fetch_page(url):
    """
    Returns the raw html nth page of updates using the given
    connection.
    """
    logging.info("Fetch {}".format(url))
    res = urllib2.urlopen(url)
    if res.code == 200:
        return res.read()


def parse_pager(url):
    pager = bs4.BeautifulSoup(fetch_page(url)).find("ul", {"class": "pager"})
    return [url + x["href"] for x in pager.find_all("a")]


def parse_tds(tds):
    repo, desc, owner, idle = tds
    return {"name": repo.get_text(), "url": repo.a["href"],
            "desc": desc.get_text(), "idle": idle.get_text()}


def parse_tr(tr):
    tr = tr.find_all("td")
    if len(tr):
        if "reposection" in tr[0]["class"]:
            return tr[0].get_text()
        elif "sublevel-repo" in tr[0]["class"]:
            return parse_tds(tr)


def parse_repolist(url):
    repos = bs4.BeautifulSoup(fetch_page(url))
    table = repos.find("table", {"summary": "repository list"})
    trs = table.find_all("tr")
    trs = (parse_tr(tr) for tr in trs)
    return [x for x in trs if x is not None]


def set_repos_category(repos):
    current = ""
    for repo in repos:
        if type(repo) is dict:
            repo["category"] = current
        else:
            current = repo


def fetch_repolist(url):
    pager = parse_pager(url)
    repos = sum([parse_repolist(page) for page in pager], [])
    set_repos_category(repos)
    return [repo for repo in repos if type(repo) is dict]


def fetch_repos_uri(url, repos):
    for repo in repos:
        page = bs4.BeautifulSoup(fetch_page(url + repo["url"]))
        uri = [a["href"] for a in page.find_all("a") if a.has_attr("href")]
        uri = [a for a in uri if a.startswith("git://")]
        if len(uri):
            repo["uri"] = uri[0]


def main():
    logging.basicConfig(level=logging.INFO)
    args = parse_args()
    repos = fetch_repolist(args.url)
    fetch_repos_uri(args.url, repos)
    with open(args.out, 'w') as f:
        repos = yaml.dump(repos, f)
    return 0

if __name__ == '__main__':
    sys.exit(main())
