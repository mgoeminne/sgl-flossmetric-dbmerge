#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys
import yaml
import shlex
import subprocess
import os
import re
import argparse

cvsanaly_cmd = "/usr/local/bin/cvsanaly2"

def exec_cmd(cmd):
    """
    Runs a shell command and redirect output, errors and input to
    stdout, stdin and stderr.
    """
    print(cmd)
    sys.stdout.flush()
    subprocess.call(shlex.split(cmd), stdin=sys.stdin,
                    stdout=sys.stdout, stderr=sys.stderr)
    sys.stdout.flush()


def extract_data(directory, repo, host, username, passwd, db):
    """
    Runs CVSAnaly on a given repository stored in a given directory.
    """
    loc = os.path.join(directory, repo["name"])
    cmd = cvsanaly_cmd + " -H {} -u {} -p {} -d {} --git-ref master "
    cmd += "'{}' --extensions=CommitsActivities"
    exec_cmd(cmd.format(host, username, passwd, db, loc))


def parse_args():
    desc = "Extract informations from git using CVSAnaly"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("-H", "--host", default="localhost",
                        help="MySQL database host name")
    parser.add_argument("-u", "--username", required=True,
                        help="MySQL database user name")
    parser.add_argument("-p", "--passwd", required=True,
                        help="MySQL database user password")
    parser.add_argument("-d", "--db", required=True,
                        help="MySQL database")
    parser.add_argument("repos",
                        help="YAML file containing repositories information")
    parser.add_argument("directory",
                        help="Directory containing git repositories")
    return parser.parse_args()


def main():
    ignore = ["bugzilla.gnome.org", "archive/bugzilla-newer"]
    args = parse_args()
    with open(args.repos) as f:
        repos = yaml.load(f)
        for repo in repos:
            if "uri" in repo and repo["name"] not in ignore:
                extract_data(args.directory, repo, args.host, args.username,
                             args.passwd, args.db)
    return 0

if __name__ == '__main__':
    sys.exit(main())
