#-*- coding: utf-8 -*-

import re

code_files_re = [
    '\.ada$', # ADA
    '\.adb$', # ADA
    '\.ads$', # ADA
    '\.asm$', # Assembler
    '\.asmx$',
    '\.asp$', # ASP
    '\.aspx$', # ASP
    '\.atf$', # Autoform
    '\.autoforms$', # Autoform
    '\.awk$', # awk
    '\.bat$', # DOS script
    '\.c$', # C
    '\.c\+\+$', # C++
    '\.c\.template$',
    '\.c\.tmpl$',
    '\.cbl$', # COBOL
    '\.cc$', # C
    '\.ccg$', # C++ files - Found in gtkmm*
    '\.cgi$', # CGI
    '\.class$', # Java
    '\.cls$', # Something to do with latex
    '\.cob$', # COBOL
    '\.cpp$', # C++
    '\.cpy$', # C++
    '\.cs$', # C#
    '\.csh$', # CShell
    '\.cxx$', # C++
    '\.defs$',
    '\.diff$', # Sometimes patches appear this way
    '\.dlg$',
    '\.dll$',
    '\.dmg$',
    '\.dtd$', # XML DTD
    '\.ec$', # C
    '\.ecp$', # C++
    '\.el$', # LISP (including Scheme)
    '\.exe$',
    '\.exp$', # Expect
    '\.f$', # FORTRAN
    '\.f77$', # FORTRAN
    '\.fd$', # Something to do with latex
    '\.font$', # Font mapping
    '\.gi$',
    '\.gidl$',
    '\.giv$',
    '\.glsl$',
    '\.gnuplot$',
    '\.gob$',
    '\.h$', # C header
    '\.h.win32$',
    '\.h\.template$',
    '\.h\.tmpl$',
    '\.hg$', # C++ header - Found in gtkmm*
    '\.hh$', # C++ header
    '\.hpp$', # C++ header
    '\.hs$', # Haskell
    '\.hxx$', # C++ header
    '\.i$',
    '\.i3$', # Modula3
    '\.idl$',
    '\.ids$', # CORBA IDL
    '\.inc$',
    '\.itk$', # TCL
    '\.jar$', # JAVA
    '\.java$', # JAVA
    '\.jl$', # LISP (including Scheme)
    '\.js$', # JavaScript (and who knows, maybe more)
    '\.l$', # (F)lex
    '\.lex$', # (F)lex
    '\.lhs$', # Haskell
    '\.ll$', # (F)lex
    '\.lsp$', # LISP (including Scheme)
    '\.m$', # Objective-C
    '\.m3$', # Modula3
    '\.mcopclass$', # MCOP IDL compiler generated class
    '\.ml$', # ML
    '\.ml3$', # ML
    '\.mm$',
    '\.nib$',
    '\.o$',
    '\.oafinfo$', # GNOME OAF
    '\.p$', # Pascal
    '\.pad$', # ADA
    '\.pas$', # Pascal
    '\.patch$',
    '\.pc$', # C
    '\.pcc$', # C++
    '\.perl$', # Perl
    '\.php(\d?)$', # PHP
    '\.pl$', # Perl
    '\.plugin$',
    '\.pm$', # Perl
    '\.pod$', # Perl
    '\.ppd$', # PDF generation
    '\.pro$', # Postscript generation
    '\.py$', # Python
    '\.py\.in$',
    '\.pyc$',
    '\.pyd$',
    '\.r$',
    '\.rb$', # Ruby
    '\.rhtml$', # eRuby
    '\.s$', # Assembly
    '\.schema$',
    '\.scm$', # LISP (including Scheme)
    '\.sed$', # Sed
    '\.sh$', # Shell
    '\.snk$',
    '\.so$',
    '\.swg$',
    '\.t$',
    '\.tcl$', # TCL
    '\.tk$', # TCL
    '\.trm$', # gnuplot term file
    '\.upd$',
    '\.vala$', # Vala
    '\.vapi$',
    '\.vbs$',
    '\.vim$', # vim syntax file
    '\.xpt$',
    '\.xs$', # Shared library? Seen a lot of them in gnome-perl
    '\.y$', # Yacc
    '\.yy$', # Yacc
    ]
code_path_re = [
    '/script/',
    '/scripts/',
    '/src/',
    ]


def compile_re(regexps):
    return [re.compile(regexp) for regexp in regexps]


files_re = [
    ('code'          , compile_re(code_files_re)),
    ]

path_re = [
    ('code'          , code_path_re),
    ]

def guess_file_type (filename):
    filename = filename.lower()
    for type, patt_list in files_re:
        for patt in patt_list:
            if patt.search(filename):
                return type

    return 'unknown'

def guess_path_type (path):
    filename = path.split('/')[-1]
    file_type = guess_file_type(filename)
    if filename != "unknown":
        return file_type
    path = path.lower()
    for type, patt_list in path_re:
        for patt in patt_list:
            if patt.search(path):
                return type

    return 'unknown'
