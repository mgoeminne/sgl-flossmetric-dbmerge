#!/usr/bin/env python
#-*- coding: utf-8 -*-


import sys
import yaml
import shlex
import subprocess
import os
import argparse


def exec_cmd(cmd):
    """
    Runs a shell command and redirect output, errors and input to
    stdout, stdin and stderr.
    """
    print(cmd)
    sys.stdout.flush()
    subprocess.call(shlex.split(cmd), stdin=sys.stdin,
                    stdout=sys.stdout, stderr=sys.stderr)
    sys.stdout.flush()


def extract_repo(repo, dest):
    """
    Extracts a git repository to a given destination.
    """
    dest = os.path.join(dest, repo["name"])
    print(dest)
    if not os.path.exists(dest):
        os.makedirs(dest)
    if os.path.exists(os.path.join(dest, ".git")):
        cwd = os.getcwd()
        os.chdir(dest)
        exec_cmd("git pull")
        os.chdir(cwd)
    else:
        exec_cmd("git clone {} '{}'".format(repo["uri"], dest))


def parse_args():
    desc = "Extract informations from git using CVSAnaly"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("repos",
                        help="YAML file containing repositories information")
    parser.add_argument("dest",
                        help="Directory where to extract git repositories")
    return parser.parse_args()


def main():
    args = parse_args()
    with open(args.repos) as f:
        repos = yaml.load(f)
        for repo in repos:
            if "uri" in repo:
                extract_repo(repo, args.dest)
    return 0

if __name__ == '__main__':
    sys.exit(main())
